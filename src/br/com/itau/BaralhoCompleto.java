package br.com.itau;

import java.util.ArrayList;

public class BaralhoCompleto {
    private ArrayList<Cartas> cartas;

    public BaralhoCompleto(ArrayList<Cartas> cartas) {
        this.cartas = cartas;
    }

    public BaralhoCompleto(){
        this.cartas = new ArrayList<>();
    }


    public void gerarBaralho() {
        String naipes[] = {"OURO","COPAS","ESPADA","PAUS"};
        String numeroCarta[] = { "As","2","3","4","5","6","7","8","9","10","Q","J","K"};
        for(int i =0; i<naipes.length;i++){
            for(int i2=0;i2<numeroCarta.length;i2++){
                gerarCarta(numeroCarta[i2],naipes[i]);
            }
        }
    }
    public void gerarCarta( String numeroCarta, String naipes){
        Cartas c = new Cartas(numeroCarta,naipes);
        cartas.add(c);
    }
}